import Document, { Html, Head, Main, NextScript } from 'next/document'

class CustomDocument extends Document {
	static async getInitialProps(ctx) {
		const initialProps = await Document.getInitialProps(ctx)
		return { ...initialProps }
	}

	render() {
		return (
			<Html className="dark">
				<Head>
					<link rel="icon" href="/identicon.svg" sizes="any" type="image/svg+xml" />
				</Head>
				<body className="bg-white dark:bg-gray-900 text-black dark:text-white">
					<Main />
					<NextScript />
				</body>
			</Html>
		)
	}
}

export default CustomDocument
