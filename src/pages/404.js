import Header from '../components/header';
import Footer from '../components/footer';
import Title from '../components/title'

const NotFound = () =>
<div>
{Header}
	<div className="m-5 text-xl text-center">
		<Title text="404" />
		<p>Oops! This page cannot be found.</p>
		<a href={'/'} className="text-blue-500 transition duration-300 ease-in-out hover:text-blue-800"> Return to Main Page.</a>
	</div>
	{Footer}
</div>;

export default NotFound;
