function Error({ statusCode }) {
	return (
		<div className="text-center">
			<p className="text-4xl">{statusCode}</p>
			<p className="text-2xl">{statusCode
				? 'An error occurred on server'
				: 'An error occurred on client'}</p>
		</div>
	)
}

Error.getInitialProps = ({ res, err }) => {
	const statusCode = res ? res.statusCode : err ? err.statusCode : 404
	return { statusCode }
}

export default Error
