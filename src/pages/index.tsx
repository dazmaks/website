import Header from '../components/header';
import Footer from '../components/footer';
import Title from '../components/title';

import Link from 'next/link';

const Index = () =>
<div className="overflow-auto">

	{Header}

	<div className="m-5 text-base md:text-lg">
		<Title text="About" />
		<p>Hello!</p>
		<p>I am a young developer and I like programming, from hardware to modern high-level languages.</p>

		<p>This <Link href="https://github.com/dazmaks/website" >
			<a target="popup" className="text-blue-800 dark:text-blue-300">website</a>
		</Link> was also made by me.</p>

		<p>Also you can <Link href="mailto:mysagaz@mail.ru" >
			<a className="text-blue-800 dark:text-blue-300">contact</a>
		</Link> me anytime you want.</p>
	</div>

	{Footer}

</div>;

export default Index;
