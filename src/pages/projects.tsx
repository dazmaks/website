import Header from '../components/header';
import Footer from '../components/footer';
import Title from '../components/title';

import Image from 'next/image';

export const projects = [
	{
		name: 'OS',
		description: 'Simple C written operating system',
		language: 'C',
		language_color: 'group-hover:text-gray-500',
		href: 'https://github.com/dazmaks/OS',
	},
	{
		name: 'Brainfuck',
		description: 'Android brainfuck interpreter',
		language: 'Java',
		language_color: 'group-hover:text-yellow-600',
		href: 'https://github.com/dazmaks/brainfuck',
	},
	{
		name: 'Identicon',
		description: 'Identicon preview tool',
		language: 'Javascript',
		language_color: 'group-hover:text-yellow-300',
		href: 'https://github.com/dazmaks/identicon',
	},
	{
		name: 'Discord token logger',
		description: 'Simple discord token logger',
		language: 'Java',
		language_color: 'group-hover:text-yellow-600',
		href: 'https://github.com/dazmaks/discord-token-logger',
	},
	{
		name: 'C# stealer',
		description: 'Simple C# stealer',
		language: 'C#',
		language_color: 'group-hover:text-green-600',
		href: 'https://github.com/dazmaks/csharp-stealer',
	},
	{
		name: 'Seam carving',
		description: 'Shitty seam carving',
		language: 'Java',
		language_color: 'group-hover:text-yellow-600',
		href: 'https://github.com/dazmaks/seam-carving',
	},
	{
		name: 'Website',
		description: 'Next.js LGBTQ+ website',
		language: 'Typescript',
		language_color: 'group-hover:text-blue-400',
		href: 'https://github.com/dazmaks/website',
	},
	{
		name: 'Rust stealer',
		description: 'My first rust project',
		language: 'Rust',
		language_color: 'group-hover:text-red-300',
		href: 'https://github.com/dazmaks/rust-stealer',
	},
	{
		name: 'Go stealer',
		description: 'My first golang project',
		language: 'Go',
		language_color: 'group-hover:text-blue-300',
		href: 'https://github.com/dazmaks/golang-stealer',
	},
	{
		name: 'The when',
		description: 'Some brainfuck scripts',
		language: 'Brainfuck',
		language_color: 'group-hover:text-pink-900',
		href: 'https://github.com/dazmaks/the-when',
	},
	{
		name: 'Elon Musk Tweets',
		description: 'My first PIL test',
		language: 'Python',
		language_color: 'group-hover:text-blue-500',
		href: 'https://github.com/dazmaks/Elon-Musk-Tweets',
	},
	{
		name: 'i686-elf-tools-opt-fix',
		description: 'Fixed crosscompiler build script',
		language: 'Shell',
		language_color: 'group-hover:text-green-400',
		href: 'https://github.com/dazmaks/i686-elf-tools-opt-fix',
	},
];

const projects_list = projects.map(project =>
	<a key={project.name} href={project.href} target="popup">
		<div className="p-5 rounded-xl text-indigo-500 dark:text-indigo-200 group transition duration-300 ease-in-out hover:text-gray-500 hover:bg-white dark:hover:text-gray-500 hover:shadow-lg hover:border-transparent">
			<p className="text-indigo-600 dark:text-indigo-300 transition duration-300 ease-in-out group-hover:text-gray-900">
				{project.name}
			</p>
			<p>{project.description}</p>
			<p className={'transition duration-300 ease-in-out '+project.language_color}>{project.language}</p>
		</div>
	</a>
);

const Projects = () =>
<div className="overflow-auto">

	{Header}

	<div className="m-5 text-base md:text-lg">
		<Title text="My projects" />
		<div className="mt-6 mb-20 flex justify-center rounded-xl overflow-hidden p-5 md:p-8 lg:p-10 bg-gradient-to-r from-indigo-50 to-indigo-200 dark:from-indigo-800 dark:to-indigo-900">
			<div className="grid gap-4 md:gap-6 lg:gap-8 grid-cols-1 md:grid-cols-2 lg:grid-cols-3">
					{projects_list}
			</div>
		</div>
	</div>

	{Footer}

</div>;

export default Projects;
