import React from 'react';

function Title(props:any):JSX.Element {
  return <p className="text-center text-2xl text-blue-800 dark:text-indigo-200">{props.text}</p>;
}

export default Title;
