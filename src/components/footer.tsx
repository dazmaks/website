const Footer =
<div className="bg-white dark:bg-gray-900 w-full p-3 bottom-0 fixed border-t dark:border-gray-500">
	<p className="font-semibold text-center text-gray-500 dark:text-gray-300"
	>made by <a href={'https://github.com/dazmaks'} target={'popup'}>dazmaks</a></p>
</div>

export default Footer;
