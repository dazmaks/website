import Image from 'next/image'

export const elements = [
	{
		name: 'Projects',
		href: '/projects',
	},
];

export const links = [
	{
		name: 'GitHub',
		src: '/github.svg',
		href: 'https://github.com/dazmaks',
	},
	{
		name: 'Discord',
		src: '/discord.svg',
		href: 'https://discord.com/users/422742277606211595',
	},
];

const header_elements = elements.map(element =>
<li key={element.name}>
	<a
		href={element.href}
		className="inline-block py-2 px-4 text-blue-500 font-semibold transition duration-300 ease-in-out hover:text-blue-800"
		>{element.name}</a>
</li>
)
const header_links = links.map(link =>
<li key={link.name}>
	<a
		href={link.href}
		target="popup"
		className="inline-block py-2 px-4"
	><Image alt={link.name} src={link.src} height={25} width={25} /></a>
</li>
)

const Header =
<ul className="flex border-b dark:border-gray-500 bg-white dark:bg-gray-900">
	<li>
		<a
			href={'/'}
			className="inline-block py-2 px-4"
		><Image alt={'Identicon'} src={'/identicon.svg'} height={25} width={25} /></a>
	</li>
{header_elements}
{header_links}
</ul>;
export default Header;
